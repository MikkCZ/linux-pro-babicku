# Co je uvnitř počítače

<!-- toc -->

## Stolní počítač
Součástky stolního počítače jsou schované v _počítačové skříni_. Stolní počítač se jmenuje stolní proto, že se skříň umisťovala na stůl, dneska teda spíš pod stůl, aby nezabíral tolik místa.

<figure>
  <a title="Sanyalox, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:1319995119_w640_h640_sistemnyj-blok-no.webp"><img width="256" alt="1319995119 w640 h640 sistemnyj-blok-no" src="../../../assets/images/pocitacova-skrin.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:1319995119_w640_h640_sistemnyj-blok-no.webp">Sanyalox</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

Počítačová skříň je krabice z plechu a plastu. Uvnitř se schovávají všechny součástky počítače. Mezi ně patří třeba:
- _základní deska_ s plošnými spoji, obvody a konektory pro zapojení ostatních součástek,
- _procesor_, který provádí výpočty,
- _operační paměť_, kde má procesor uložené právě běžící programy a výpočty,
- _pevný disk_ se soubory (fotkami, videi, dokumenty), ale i programy připravenými ke spuštění,
- _elektrický zdroj_, který je kabelem zapojený do zásuvky a menšími kablíky pak každé součástce dodává správné elektrické napětí.

Uvnitř to pak celé vypadá nějak takto:
<figure>
  <a title="DonES, CC BY-SA 3.0 &lt;http://creativecommons.org/licenses/by-sa/3.0/&gt;, prostřednictvím Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Silent_PC-Antec_P180.JPG"><img width="256" alt="Silent PC-Antec P180" src="../../../assets/images/pocitacova-skrin-uvnitr.jpg"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Silent_PC-Antec_P180.JPG">DonES</a>, <a href="http://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

## Notebook neboli laptop
Druhým používaným typem počítače je _notebook_ nebo _laptop_ 💻. Oba názvy označují totéž. Notebook má všechny součástky a i některá [periferní zařízení](periferní-zařízení.md) integrovaná přímo ve svém těle. Je menší, přenosný a nemusí být ani trvale připojený do zásuvky, protože má vlastní baterku.

<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="../../../assets/images/Lenovo-V330.jpg"><img width="256" alt="Lenovo V330 (1)" src="../../../assets/images/Lenovo-V330-256px.jpg"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Lenovo_V330_(1).jpg">Michal Stanke</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

## Co se dá k počítači připojit
Zvenčí na počítačové skříni nebo těle notebooku najdeš různé [konektory](periferní-zařízení.md#konektory), do kterých můžeš přímo nebo přes kabel připojovat [periferní zařízení](periferní-zařízení.md).
