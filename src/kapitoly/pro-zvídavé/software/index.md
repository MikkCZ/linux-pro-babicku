# Software - jak fungují programy

Software (&bdquo;soft&ldquo; angl. měkký nebo hebký) jsou počítačové programy. To znamená všechno, co vidíš na obrazovce, a ještě trochu víc, co není ani vidět ani si na to nemůžeš sáhnout. Pokud byl [hardware](../hardware/index.md) takovým tělem počítače, software je jeho myslí, která ho řídí.

- [Operační systém](operační-systém.md)
- [Programy](programy.md)
- [Licence](licence.md)
