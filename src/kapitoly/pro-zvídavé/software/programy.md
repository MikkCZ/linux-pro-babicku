# Programy

Uživatelské programy jsou další programy, které můžeš za pomoci [operačního systému](operační-systém.md) spouštět. Operační systém má v sobě předinstalované základní programy pro nastavení počítače, správce souborů, kalkulačku, někdy i kalendář a [webový prohlížeč](../../internet/webový-prohlížeč.md), a další si můžeš doinstalovat.

<!-- toc -->

## Instalace
Programy jsou na počítači uložené v [souborech](../../soubory/index.md) stejně jako fotky nebo dokumenty. Když instaluješ nějaký program, zkopírují se jeho soubory do počítače tak, aby je operační systém rovnou sám našel, případně se mu ještě řekne, kde je má hledat.

## Jak programy vznikají
Každý program vzniká z textu, kterému se říká [zdrojový kód](#zdrojový-kód). Tyto texty píší programátoři v programovacích jazycích.

### Zdrojový kód
Zdrojový kód je text, ve kterém pomocí programovacího jazyka programátor popsal, co má program dělat a jak má vypadat. Některým programovacím jazykům počítač a operační systém rozumí rovnou, z jiných jazyků se musí program před spuštěním nejdřív automaticky &bdquo;přeložit&ldquo;.

Zdrojový kód může vypadat třeba takhle:
```python
# nadefinuje, jak se sčítají dvě čísla označená jako 'x' a 'y'
def secti_cisla(x, y):
    return x + y

# využije nadefinované 'secti_cisla' pro sečtení 123 a 456
# výsledek uloží pod písmenko 'z'
z = secti_cisla(123, 456)

# napíše výsledek uložený pod písmenkem 'z', tedy 579
print(z)
```

Když je zdrojový kód programu veřejně dostupný a jeho licence dovoluje každému se na něj podívat a používat ho, říká se, že je ten program [open-source nebo svobodný](licence.md#svobodná-licence). Pokud je zdrojový kód tajný, nikdo k němu nemůže a licence programu obsahuje podmínky pro jeho používání, říká se mu [proprietární](licence.md#proprietární-software).
