# Velikost souborů

Soubory se v počítači ukládají jako jedničky a nuly. Jejich počtem se tak měří i jejich velikost. Každá jednička nebo nula se označuje jako _bit_ (angl., čti &bdquo;byt&ldquo;), a každých osm jedniček a nul (osm bitů) dává dohromady jeden _byte_ (angl., česky &bdquo;bajt&ldquo;). Před _bajt_ se pak dávají předpony _kilo_ (tisíc), _mega_ (milion), _giga_ (miliarda) atd.

To znamená, že:
- _Kilobajt_ (zkratka &bdquo;kB&ldquo;) je osm tisíc jedniček nebo nul.<br>
Desítky nebo stovky kilobajtů mají webové stránky o obrázky na nich.
- _Megabajt_ (zkratka &bdquo;MB&ldquo;) je osm milionů jedniček nebo nul.<br>
Jednotky megabajtů mají třeba fotky, desítky a stovky megabajtů bývají velké programy. Na disketu se vešel přibližně megabajt a půl.
- _Gigabajt_ (zkratka &bdquo;MB&ldquo;) je osm miliard jedniček nebo nul.<br>
Tak velké jednotlivé soubory většinou ani nejsou. Na CD se vejde asi tři čtvrtě gigabajtu, na DVD necelých pět gigabajtů, kapacita USB flešek a pevných disků bývá v desítkách, stovkách nebo tisících gigabajtů. Jednotky gigabajtů jsou také většinou limitem, kolik dat nám dovolí mobilní operátor měsíčně stáhnout nebo poslat přes mobilní internet v chytrém telefonu.
