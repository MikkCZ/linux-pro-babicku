# Internet jako síť počítačů

<!-- toc -->

## Jak se počítače na internetu najdou
Když zadáš do webového prohlížeče nějakou stránku, třeba _google.com_, jako první se začne hledat adresa [serveru](servery.md), na kterém je stránka uložená. Názvu _google.com_ se říká _doména_ a tu si dobře pamatujeme my lidé. Počítače ale používají číselné kombinace nazvané _IP adresy_. Pro zjištění správné IP adresy pro požadovanou doménu se automaticky použije internetová služba _DNS_. Se zjištěnou IP adresou už se pak dokáže prohlížeč se serverem spojit a stránku z něj stáhnout a zobrazit.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/EdIyfV4AC0M" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Z kolika serverů a dalších počítačů se internet skládá
Na tuhle otázku není jednoduchá odpověď. Je jich ale určitě více než 4 miliardy. Ve videu Roman Zach zmiňuje, že neobsazené _IPv4_ adresy docházejí. Těch jsou 4 miliardy a od natočení videa už opravdu došly. Těch počítačů je ale mnohem více, protože o jednu IP adresu se může počítačů dělit víc. Navíc jsme ani nezapočítali domácí počítače jako ten, ze kterého se k internetu připojuješ ty, a který taky nemusí mít svou vlastní IP adresu.
