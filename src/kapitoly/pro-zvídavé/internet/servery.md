# Server

Server je počítač, který poskytuje skrze síť internetu nějakou službu. Na serverech jsou uložené webové stránky a soubory, které stahuješ, a servery pro tebe posílají a přijímají e-mailové zprávy.

## Jak server vypadá
Název &bdquo;server&ldquo; neoznačuje ani tak typ počítače, jako jeho funkci. Jako server může sloužit i běžný [stolní počítač](../hardware/uvnitř-počítače.md#stolní-počítač) nebo chytrý mobilní telefon. Stačí být jen správně připojený k internetu a mít na počítači nastavenou službu, kterou chceš poskytovat.

Protože se servery používají pro konkrétní účel, většinou nakonec ze specializovaných součástek postavené jsou. Servery se umisťují do specializovaných místností a budov, kterým se říká _datové sály_ nebo _datacentra_, kde mají pro svoje fungování zajištěno všechno potřebné (elektřinu, studený vzduch pro chlazení, rychlé internetové připojení). V datacentrech jsou servery pro úsporu prostoru ve skříních, a do každé se jich naskládá třeba i 40.

<figure>
  <a title="Truenetworks, CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, prostřednictvím Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:NUCserver_universal_solution_7.jpg"><img width="256" alt="NUCserver universal solution 7" src="../../../assets/images/serverova-skrin.jpg"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:NUCserver_universal_solution_7.jpg">Truenetworks</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0">CC BY-SA 3.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>
