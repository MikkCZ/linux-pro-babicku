# Další webové stránky a služby

V kapitole o [webových stránkách a službách](../../internet/webové-stránky-a-služby.md) jsem slíbil, že jich je ještě mnohem víc. Ani tady nenajdeš víčet všech - je jich opravdu hodně a každý den další přibývají. Najdeš tu ale některé trochu více specializované.

<!-- toc -->

## Encyklopedie
Díky tomu, že internet umožňuje spojit se s lidmi na druhém konci naší planety, můžou také spolupracovat na tvorbě stránek. Příkladem výsledku takové spolupráce, kde mnoho lidí na jeden web sdílí své znalosti, jsou internetové encyklopedie, někdy nazývané také &bdquo;wiki&ldquo;. Tyto encyklopedie se mohou věnovat buď něčemu velmi konkrétnímu (např. fanouškovské wiki různých filmů a seriálů, nebo motoristické), nebo jsou zcela obecné.

Největší internetovou encyklopedií je [Wikipedie](https://cs.wikipedia.org/), která má jen v češtině půl milionu článků, v angličtině dokonce více než desetkrát tolik. Najdeš tam informace o chemických sloučeninách, zvířatech, historii, slavných osobnostech, umění a na co všechno dalšího si vzpomeneš.

<figure>
  <a title="ClintEastwood Blondie, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:9.27.2017_Wikipedia_English_Homepage.png"><img width="512" alt="9.27.2017 Wikipedia English Homepage" src="../../../assets/images/wikipedie.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:9.27.2017_Wikipedia_English_Homepage.png">ClintEastwood Blondie</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

## Překladače a slovníky
Protože už je dneska na internet připojený skoro celý svět, nemluví se na něm jedním jazykem. Některé stránky jsou česky, většina anglicky, a další jinými jazyky. Pro je na internetu také hodně služeb, které umí zadané slovo, text nebo celou webovou stránku přeložit téměř z jakéhokoliv jazyka do jiného. Velmi dobrý je v tom [překladač od Googlu](https://translate.google.com/).

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/scOZS_xlRjc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Blogy
Název &bdquo;blog&ldquo; vznikl z původního &bdquo;web log&ldquo; (angl. webový zápisník) a jsou to většinou osobní stránky lidí, kteří na nich sdílí svoje názory a pro ně nové věci nebo zajímavosti. Některé blogy je těžší najít, třeba ten můj na adrese [michal.stanke.cz](https://michal.stanke.cz/), ale prostor pro osobní zápisky a názory nabízí i zpravodajské a publicistické weby jako iDnes (online varianta MF DNES).

## Sociální sítě
Pro sdílení informací, ať už textů, fotek nebo videí, ze svého života používá hodně lidí sociální sítě. I když nejsi žádná celebrita, můžeš si vytvořit účet na některé ze sociálních sítích a veřejně tam psát vtipné hlášky nebo fylozofické úvahy a diskutovat s ostatními lidmi. Hodně sítí nabízí možnost soukromého chatování (psaní krátých zpráv) s přáteli, ale většinou na nich jde právě o to vyjádřit svůj názor veřejně, nebo na takový názor reagovat.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/5B2ud6YSy_g" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Největší sociální sítě jsou Facebook (čti &bdquo;fejsbůk&ldquo;), Instagram (čti jak je psáno), Twitter (čti &bdquo;tvitr&ldquo;) nebo TikTok.

## Dezinformační weby
Protože na web může napsat kdo chce a skoro co chce, vznikají i takzvané dezinformační weby. Bývají věnované hodně politickým tématům. A buď publikují vlastní interpretace informací, nebo často úplné lži, většinou s cílem ovlivnit názory veřejnosti.

Ani vzhled stránky nemusí nutně vypovídat o její důvěryhodnosti. Ty nejlepší dezinformační weby vypadají nachlup stejně jako seriózní portály velkých světových novin. Je potřeba si proto velmi důkladně dávat pozor na to, jaké informace na webových stránkách čteme, a pokud zní až moc fantasticky nebo naopak strašidelně, ověřit si je i jinde.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/LvmCeg7RaFQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
