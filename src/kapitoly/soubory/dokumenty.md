# 📄 Dokumenty

V počítači potkáš mnoho typů dokumentů. V této kapitole se podíváme na ty, které obsahují text.

<!-- toc -->

## Formáty dokumentů s textem
Na internetu budeš nacházet dokumenty _PDF_. Poznáš je podle ikony <i class="fa fa-file-pdf-o" aria-hidden="true" style="color:red"></i> a toho, že nejdou měnit. Nejde do nich nic připsat ani z nich nic mazat.

Pro vytvoření vlastního dokumentu nebo prohlížení a úpravu takového, který upravovat jde, použijeme program _LibreOffice Writer_, který funguje na [operačních systémech](../pro-zvídavé/software/operační-systém.md) Linux i Windows. S Windows bychom mohli ještě použít programy _WordPad_ nebo _Microsoft Office Word_, jejichž ovládání je prakticky totožné.

Po spuštění _Writeru_ uvidíš okno s bílou plochou jako list papíru, do které můžeš klepnout myší a psát.

<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="../../assets/images/Linux-Mint-20-MATE-writer.png"><img width="512" alt="Linux-Mint-20-MATE-writer" src="../../assets/images/Linux-Mint-20-MATE-writer-512px.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Linux-Mint-20-MATE-writer.png">Michal Stanke</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

## Práce s textem

### Kopírování textu
Pokud třeba na [webové stránce](../internet/webový-prohlížeč.md), v [e-mailu](../internet/e-maily.md) nebo právě v dokumentu uvidíš nebo máš napsaný text, který chceš někomu poslat nebo si uložit do jiného dokumentu, nemusíš ho opisovat, ale můžeš ho tam zkopírovat.

Jako první označ text myší.
1. Najeď myší na začátek textu až se změní kurzor na <i class="fa fa-i-cursor" aria-hidden="true"></i>.
1. Stiskni (a drž) levé tlačítko myši.
1. Přejeď kurzorem na konec textu, který chceš kopírovat. Ten se zvýrazní tmavší barvou.
1. Tlačítko myši pusť.

Teď můžeš pro zkopírování textu použít klávesovou zkratku `Ctrl`+`C`, nebo na označený text klepnout pravým tlačítkem myši a z nabídnutých možností vybrat &bdquo;Kopírovat&ldquo;. Někdy je tato možnost označená ikonkou <i class="fa fa-clone" aria-hidden="true"></i>.

Teď už otevři okno nebo najdi místo, kam chceš text zkopírovat. Klepni levým tlačítkem myši na místo, kam chceš text vložit, aby se objevil blikající kurzor <i class="fa fa-i-cursor" aria-hidden="true"></i> pro psaní, a pak můžeš opět použít klávesovou zkratku, tentokrát `Ctrl`+`V`. Nebo na to místo klepni pravým tlačítkem myši a z nabídnutých možností vyber &bdquo;Vložit&ldquo;. Tahle možnost může být označená ikonkou <i class="fa fa-clipboard" aria-hidden="true"></i>.

### Klávesnice
Klávesnice funguje při práci v dokumentech stejně jako jinde v počítači a jak jsem zmiňoval v kapitole [Ovládání počítače](../ovládání-počítače.md#jak-psát-na-klávesnici). Pro mazání textu mám ale ještě jeden tip navíc. Pokud potřebuješ smazat větší kus, třeba celou větu nebo odstavec, označ ho myší jako při kopírováním a stiskni klávesu `Backspace`.

Klávesové zkratky se hodí nejenom pro kopírování a vkládání textu. V programech pro práci s textem fungují všechny [běžné klávesové zkratky](../pro-zvídavé/klávesové-zkratky.md) a některé další k tomu:
- `Ctrl`+`F` zobrazí políčko pro hledání slova v dokumentu,
- `Caps Lock` změní velikost psaných písmen na velká. Na malá je nastavíš zpět dalším stisknutím.
- `Shift`+`písmeno` napíše toto písmeno veliké. Puštěním `Shift` budeš dál psát zase malá písmena.
- `Ctrl`+`S` uloží dokument nebo provedené změny do souboru,
- `Ctrl`+`P` otevře okno pro spuštění tisku dokumentu.

### Uložení dokumentu
Během psaní dokumentu si ho určitě průběžně ukládej do souboru. Kdyby se počítač třeba z jakéhokoliv důvodu vypnul, o neuložený dokument přijdeš. Nejrychlejší způsob, jak dokument uložit, je výše uvedená zkratka `Ctrl`+`S`. Jde to ale také z nabídky &bdquo;Soubor&ldquo; v levém horním rohu okna. Dokument si samozřejmě ulož i po jeho úplném dokončení, než program zavřeš.

<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;" href="../../assets/images/Linux-Mint-20-MATE-writer-save.png"><img width="400" alt="Linux-Mint-20-MATE-writer-save.png" src="../../assets/images/Linux-Mint-20-MATE-writer-save-crop-400px.png"></a>
</figure>

Dokument si můžeš navíc uložit i jako _PDF_, to je ten, co nejde měnit, s ikonkou <i class="fa fa-file-pdf-o" aria-hidden="true" style="color:red"></i>. To se hodí hlavně ve chvíli, kdy ho budeš chtít někomu poslat. Pro uložení do PDF v nabídce &bdquo;Soubor&ldquo; v levém horním rohu okna najeď myší o něco níže na &bdquo;Exportovat jako&ldquo; a v malé podnabídce, která se otevře, &bdquo;Exportovat do PDF…&ldquo;.

## Další typy dokumentů
Protože je typů dokumentů je celá řada, všechny by se do jedné kapitoly popsat nevešly. V kapitolách &bdquo;pro zvídavé&ldquo; tedy najdeš informace ještě o [tabulkách](../pro-zvídavé/soubory/tabulky.md) a [prezentacích](../pro-zvídavé/soubory/prezentace.md).

Tato kapitola nebyla první, kde jsem zmínil internet nebo webových prohlížeč. Pojďme se tedy podívat, [jak na internet](../internet/index.md).
