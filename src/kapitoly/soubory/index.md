# 📁 Práce se soubory

Některé soubory najdeš jako ikony přímo na ploše po zapnutí počítače, nebo ve složkách. Ikony tvých souborů vypadají většinou jako list papíru 📄 nebo malý obrázek 🖼️. Složky poznáš podle ikony podobné složce nebo šanonu na papíry 📁.

<!-- toc -->

## Co všechno je v souborech
_Fotka_, _video_, _dokument_ i _program_, to všechno je v počítači nebo na [paměťovém médiu](../pro-zvídavé/hardware/periferní-zařízení.md#paměťová-média) uložené jako soubor.

## Jak otevřít soubor
_Soubor_ i _složku_ otevřeš dvojím klepnutím levým tlačítkem myši, nebo označením (jedno klepnutí) a stisknutím klávesy `Enter`. Pokud je soubor programem, těmito způsoby ho spustíš.

Soubory se otevírají pomocí programů, které s nimi umí pracovat. Obrázky umí zobrazit jiný program než video a další program zase rozumí dokumentům. O některých se dočteš v dalších kapitolách. Obsah složky ti zase ukáže okno správce souborů.

## Kopírování a přesouvání souborů
Pro lepší orientaci můžeš soubory _přesouvat_ na jiná místa a do jiných složek.
1. Najeď myší na ikonu souboru.
1. Stiskni (a drž) levé tlačítko myši.
1. Posunem myši přetáhni soubor na požadované místo.
1. Tlačítko myši pusť.

_Kopírování_ souborů funguje obdobně jako [kopírování textu](dokumenty.md#kopírování-textu). Jen pro označení souboru klepni jednou na jeho ikonu. Pokud jich chceš označit více najednou, při klepnutí na další soubor drž na klávesnici stisknutou klávesu `Ctrl`, nebo označ všechny zobrazené soubory pomocí [klávesové zkratky](../pro-zvídavé/klávesové-zkratky.md) `Ctrl`+`A`.

<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="../../assets/images/Linux-Mint-20-MATE-nemo-window.png"><img width="512" alt="Linux-Mint-20-MATE-nemo-window" src="../../assets/images/Linux-Mint-20-MATE-nemo-window-crop-512px.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Linux-Mint-20-MATE-nemo-window.png">Michal Stanke</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

Přesouvání souborů je rychlé, jako když přendáš knížku na jinou poličku. Při kopírování ale může chvilku trvat, než počítač obsah celého souboru přečte a &bdquo;opíše&ldquo; jeho kopii. Jeden soubor nebo složka může obsahovat knížky z celé knihovny nebo fotky z celého fotoalba. [Jak velké vlastně soubory v počítači můžou být?](../pro-zvídavé/soubory/velikost-souborů.md)

## Jak soubor smazat
Nepotřebné nebo nechtěné soubory je potřeba z počítače vymazat, aby v něm nezabíraly místo a nedostal se k nim nikdo, kdo nemá. Své ti [o tom může povědět Kuba z Datové Lhoty](https://www.ceskatelevize.cz/ivysilani/11933175266-datova-lhota/217543112110004-koukej-mazat).

<figure>
  <a title="Datová Lhota: 4. Koukej mazat! — iVysílání — Česká televize" href="https://www.ceskatelevize.cz/ivysilani/11933175266-datova-lhota/217543112110004-koukej-mazat" target="_blank"><img width="512" alt="Datová Lhota: 4. Koukej mazat! — iVysílání — Česká televize" src="../../assets/images/datova-lhota-disk.jpg"></a>
  <figcaption><small>
    <a href="https://decko.ceskatelevize.cz/datova-lhota" target="_blank">Datová Lhota</a> — Déčko — Česká televize
  </small></figcaption>
</figure>

Pro smazání souboru máš několik možností:
1. Klepni na ikonu souboru pravým tlačítkem myši a v zobrazené nabídce vyber &bdquo;Smazat&ldquo;. Tato možnost bývá označená ikonkou 🗑️ nebo <i class="fa fa-times" aria-hidden="true" style="color:red"></i>.
1. Klepni na ikonu souboru jednou levým tlačítkem myši a stiskni klávesu `Delete`.
1. Přesuň soubor, jak je popsáno výše, do speciální složky &bdquo;Koš&ldquo;, kterou najdeš na ploše.

Tím jsme soubor ale jenom vyhodili do koše, v počítači vlastně zůstal. Abychom se ho úplně zbavili, klepni pravým tlačítkem myši na ikonu složky &bdquo;Koš&ldquo; a v nabídce vyber &bdquo;Vyprázdnit&ldquo; nebo &bdquo;Vysypat&ldquo;.

## Vytváření souborů
V následující kapitole se podíváme, jak soubory vytvářet, a [začneme s dokumenty](dokumenty.md).
