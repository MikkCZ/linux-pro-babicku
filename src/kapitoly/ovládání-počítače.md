# 🖱️ Ovládání počítače

Základní funkce počítače se ovládají pomocí myši 🖱️, která pohybuje kurzorem <i class="fa fa-mouse-pointer" aria-hidden="true"></i> na monitoru 🖥️, a klávesnice ⌨️ pro psaní textu. I tohle možná znáš a můžeš rovnou postoupit [na konec kapitoly](#už-to-umím).

<!-- toc -->

## Co uvidím po spuštění
[Zapnutí počítače](zapnutí-počítače.md) trvá asi minutu nebo dvě. Až se přestanou ukazovat loga výrobce, uvidíš na monitoru připravenou _pracovní plochu_, která bude vypadat asi takto:

<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="../assets/images/Linux-Mint-20-MATE-desktop.png"><img width="512" alt="Linux-Mint-MATE-20-desktop" src="../assets/images/Linux-Mint-20-MATE-desktop-512px.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Linux-Mint-MATE-20-desktop.png">Michal Stanke</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

## Pohyb myší
Na ploše najdi _kurzor myši_ ve tvaru malé šipky <i class="fa fa-mouse-pointer" aria-hidden="true"></i>. Pohybovat jím můžeš tak, že položíš ruku na myš a posuneš jí po stole. Napoprvé to může chtít chvilku cviku, aby se kurzor na monitoru posunul na tebou vybrané místo.

<figure>
   <i class="fa fa-mouse-pointer" aria-hidden="true"></i>
   <br>
   <span style="margin-left:1em; font-size:3em;">🖱️</span>
</figure>

## Hlavní panel
_Hlavní panel_ je nízká lišta nebo pruh, který se táhne po spodní hraně monitoru. Úplně vlevo na hlavním panelu je tlačítko &bdquo;Menu&ldquo; nebo &bdquo;Start&ldquo;, kde se schovávají všechny nainstalované programy, nastavení počítače a taky tlačítko pro vypnutí počítače.

Zkus posunout kurzor myši tak, aby špičkou ukazoval na tlačítko &bdquo;Menu&ldquo;, klepnout na něj krátkým stisknutím levého tlačítka na myši, a podívat se, jak to tam vypadá. Levé tlačítko slouží pro výběr věcí, které chceš použít nebo zapnout.

<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="../assets/images/Linux-Mint-20-MATE-menu.png"><img width="512" alt="Linux-Mint-20-MATE-menu" src="../assets/images/Linux-Mint-20-MATE-menu-crop-512px.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Linux-Mint-20-MATE-menu.png">Michal Stanke</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

## Ikony a spouštění programů
Vedle tlačítka &bdquo;Menu&ldquo; máš připravené _ikony_ pro spuštění svých nejpoužívanějších programů. Najeď teď kurzorem myši na ikonu [webového prohlížeče](internet/webový-prohlížeč.md) Firefox <i class="fa fa-firefox" aria-hidden="true"></i> a klepni na ni.

<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="../assets/images/Linux-Mint-20-MATE-toolbar-icon.png"><img width="512" alt="Linux-Mint-20-MATE-toolbar-icon" src="../assets/images/Linux-Mint-20-MATE-toolbar-icon-crop-512px.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Linux-Mint-20-MATE-toolbar-icon.png">Michal Stanke</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

## Okna programů
Výborně, jakmile se na ploše objeví takovéto _okno_, máš spuštěný první program.

<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="../assets/images/Linux-Mint-20-MATE-Firefox-window.png"><img width="512" alt="Linux-Mint-20-MATE-Firefox-window" src="../assets/images/Linux-Mint-20-MATE-Firefox-window-crop-512px.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Linux-Mint-20-MATE-Firefox-window.png">Michal Stanke</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

V pravém horním rohu, úplně na kraji okna, jsou tři tlačítka. Zleva:
- <i class="fa fa-window-minimize" aria-hidden="true"></i> při klepnutí na první tlačítko okno skryješ dolů do hlavního panelu. Pro jeho opětovné zobrazení na něj v panelu klepni.
- <i class="fa fa-window-maximize" aria-hidden="true"></i> při klepnutí na druhé tlačítko celé okno zvětšíš a vyplní celou obrazovku,
- <i class="fa fa-times" aria-hidden="true"></i> a klepnutím na to poslední tlačítko okno a případně celý program zavřeš a vypneš.

<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="../assets/images/Linux-Mint-20-MATE-Firefox-window.png"><img width="512" alt="Linux-Mint-20-MATE-Firefox-window" src="../assets/images/Linux-Mint-20-MATE-Firefox-window-close-crop-512px.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Linux-Mint-20-MATE-Firefox-window.png">Michal Stanke</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

Oken spuštěných programů můžeš mít najednou víc vedle sebe nebo i přes sebe. Každé okno se zobrazí nejenom na ploše, ale jeho název uvidíš i v prostřední části hlavního panelu, kde je jejich seznam. Najetím kurzoru myši a klepnutím na název v seznamu se okno skryje, nebo zase zobrazí.

<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="../assets/images/Linux-Mint-20-MATE-Firefox-window.png"><img width="512" alt="Linux-Mint-20-MATE-Firefox-window" src="../assets/images/Linux-Mint-20-MATE-Firefox-window-toolbar-crop-512px.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Linux-Mint-20-MATE-Firefox-window.png">Michal Stanke</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

Vyzkoušej si skrývání a opětovné zobrazování okna a nakonec program zavři.

## Jak psát na klávesnici
Popis používání _klávesnice_ ⌨️ si dovolím v této příručce vynechat. Budeš ji dobře znát, protože se na ní píše jako na psacím stroji. Pro výběr místa, kam chceš psát, na něj najeď kurzorem myši a klepni levým tlačítkem. Na místě psaní bude blikat druhý kurzor <i class="fa fa-i-cursor" aria-hidden="true"></i> jako třeba tady dole v té nabídce.

<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="../assets/images/Linux-Mint-20-MATE-menu.png"><img width="512" alt="Linux-Mint-20-MATE-menu" src="../assets/images/Linux-Mint-20-MATE-menu-crop-512px.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Linux-Mint-20-MATE-menu.png">Michal Stanke</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

Snad jen oproti psacímu stroji jde na počítači napsaný text snadno mazat klávesou `Backspace`. Na klávesnici bývá vpravo nahoře přibližně nad šipkami a má na sobě jednu dlouhou šipku <i class="fa fa-long-arrow-left" aria-hidden="true"></i>.

Klávesnice nabízí taky možnost používat _klávesové zkratky_, o kterých si můžeš [přečíst v kapitole &bdquo;pro zvídavé&ldquo;](pro-zvídavé/klávesové-zkratky.md).

## Už to umím
Jde ti to skvěle. Můžeš si dát přestávku a počítač vypnout v nabídce &bdquo;Menu&ldquo; na [hlavním panelu](#hlavní-panel), nebo si naopak přečíst podrobnosti, [jak tohle všechno funguje](pro-zvídavé/software/index.md). Nejsi na dlouhé čtení? Pak tu pro tebe mám kapitolu o [práci se soubory](soubory/index.md).
