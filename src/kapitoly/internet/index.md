# 🌐 Internet

Internet je velká síť propojující mnoho a mnoho počítačů, které spolu komunikují. Díky internetu si můžeš zobrazovat webové stránky, stahovat soubory, posílat e-maily, telefonovat přes programy jako Skype apod. Internet je opravdu velký, ale nemusíš se ho vůbec bát.

<!-- toc -->

## Připojení k internetu
Pro přístup k internetu je potřeba mít k němu připojení. To ti dá (pronajme) _poskytovatel připojení k internetu_ nebo mobilní operátor. Způsoby připojení se podle toho mohou lišit. Internet může jít kabelem, bezdrátově přes Wi-Fi a mobilní datovou síť, nebo jejich kombinací.

Nejčastěji na internetu prohlížíš webové stránky, a k tomu budeš potřebovat [webový prohlížeč](webový-prohlížeč.md).
