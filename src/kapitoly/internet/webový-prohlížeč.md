# <i class="fa fa-firefox" aria-hidden="true"></i> Webový prohlížeč

Webový prohlížeč je tvůj asi nejpoužívanější program. Funguje jaké brána k webovým stránkám a pokaždé si tak můžeš dohledat nové informace nebo přečíst, co se právě děje ve světě. Webové stránky jsou soubory uložené na [serverech](../pro-zvídavé/internet/servery.md), odkud je tvůj prohlížeč umí přes internet stáhnout a zobrazit (načíst).

<!-- toc -->

## Základní ovládání
Stejně jako u jiných programů i prohlížečů je celá řada. Jako příklad se podíváme na Firefox <i class="fa fa-firefox" aria-hidden="true"></i>, ale ostatní vypadají a fungují stejně nebo velmi velmi podobně.

<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="../../assets/images/Linux-Mint-20-MATE-Firefox-window.png"><img width="512" alt="Linux-Mint-20-MATE-Firefox-window" src="../../assets/images/Linux-Mint-20-MATE-Firefox-window-crop-512px.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Linux-Mint-20-MATE-Firefox-window.png">Michal Stanke</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

Hlavní ovládací prvek prohlížeče je _adresní řádek_. Do něj se zadává adresa webové stránky, kterou chceš navštívit např. _google.com_. Vedle najdeš také šipky ⬅️ a ➡️, kterými se můžeš vracet na předtím zobrazené stránky, domeček 🏠 pro návrat na domovskou stránku, a tlačítko se zatočenými šipkami <i class="fa fa-refresh" aria-hidden="true"></i>, které stránku načte znovu od začátku, třeba když se něco nepovede. Nejvíce prostoru je pak necháno pro obsah samotné stránky, aby se dobře četla.

## Práce s textem a zkratky
Psaní textu na webových stránkách, které to umožňují, funguje [stejně jako v dokumentech](../soubory/dokumenty.md#práce-s-textem), i [klávesové zkratky](../pro-zvídavé/klávesové-zkratky.md) tu fungují stejně jako jinde. Některé další, které se ti tu budou hodit, jsou:
- `Ctrl`+`F` zobrazí políčko pro hledání slova v obsahu stránky,
- `F5` ve webovém prohlížeči znovu načte zobrazenou stránku, třeba pokud jí kousek chybí (je to stejná jako klepnutí na tlačítko <i class="fa fa-refresh" aria-hidden="true"></i>),

## Jak hledat a ukládat webové stránky
Jak se ale na stránky dostat? Pokud znáš jejich adresu, stačí ji napsat do zmiňovaného adresního řádku (toho velkého políčka nahoře). Pokud ne, můžeš stránky a informace na webu hledat pomocí [internetových vyhledávačů](webové-stránky-a-služby.md#vyhledávače).

Když už si prohlížíš nějakou zajímavou stránku a víš, že se na ni budeš chtít podívat i zítra nebo třeba za týden, nemusíš se její adresu učit nazpaměť ani si ji opisovat. Místo toho má každý prohlížeč _záložky_. Stránku si založíš klepnutím na hvězdičku ⭐ v pravém konci adresního řádku. Záložky vidíš na liště pod adresním řádkem a pokud na některou klepneš, prohlížeč její adresu do adresního řádku vloží a webovou stránku znovu načte.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/stREiU0qIJs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Až si vyzkoušíš ovládání prohlížeče, podíváme se na to, jaké všechny [webové stránky](webové-stránky-a-služby.md) na internetu najdeš.
