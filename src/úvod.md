# Úvod do Linuxu pro babičku 👵

Doufám, že ti příručka poslouží jako přehled, co v počítači najdeš, co je ten Linux, jaké programy v počítači očekávat a k čemu se ti mohou hodit. Pokud si ji budeš číst na internetu, vlevo najdeš seznam všech kapitol. Listovat mezi nimi můžeš také šipkami na klávesnici ⬅️ a ➡️.

Na konci některých kapitol najdeš odkazy na kapitoly &bdquo;pro zvídavé&ldquo; s podrobnějšími a trochu odbornějšími informacemi o tom proč a jak co funguje. Ale opravdu jen trochu, nemusíš se jich bát.

## Pro ostatní návštěvníky
Tuto příručku jsem [začal psát](https://michal.stanke.cz/2021/01/24/linux-pro-babicku.html) hlavně pro svou babičku jako &bdquo;co je co&ldquo; v počítači. Babička umí počítač zapnout, zvládá e-maily, trochu vyhledávat na internetu a &bdquo;skajpovat&ldquo;. Někdy ji ale zajímá, jaký je rozdíl mezi její e-mailovou schránkou a internetem, proč nejde nový monitor do počítače zapojit starým kabelem, nebo jestli při tom nepřijde o uložené fotky, a moji rodiče jí na to bohužel nedokážou vždy odpovědět.

Nechci z babičky udělat odborníka na sestavování počítačů ani programátora, ale chci pro ni připravit jednoduché základní informace, kterým porozumí a odpoví jí na další otázku, která ji při používání počítače napadne. Proto může být obsah příručky místy zjednodušený a nemusí nutně vyhovovat vašim potřebám. Přesto budu rád, pokud mi [navrhnete jakékoliv změny](https://gitlab.com/MikkCZ/linux-pro-babicku), které budete považovat za vhodné.

### Licence
[![licence](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)

Toto dílo podléhá licenci [Creative Commons Uveďte původ-Zachovejte licenci 4.0 Mezinárodní License](https://creativecommons.org/licenses/by-sa/4.0/).
