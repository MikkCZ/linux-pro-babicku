CARGO_INSTALL_ROOT=.cargo
export PATH := $(CARGO_INSTALL_ROOT)/bin:$(PATH)

.DEFAULT_GOAL := all
.PHONY: all
all: prepare build

.PHONY: clean
clean:
	rm -rf .cargo book

.PHONY: prepare
prepare:
	cargo install --locked --root "$(CARGO_INSTALL_ROOT)" mdbook mdbook-toc cargo-deadlinks
	mdbook --version
	mdbook-toc --version
	deadlinks --version

.PHONY: preview
preview:
	mdbook serve --open

.PHONY: build
build:
	mdbook build --dest-dir book
	deadlinks --verbose book

.PHONY: all_in_podman
all_in_podman: prepare_in_podman build_in_podman

.PHONY: prepare_in_podman
prepare_in_podman:
	bash ./scripts/run_in_podman.sh 'make prepare'

.PHONY: preview_in_podman
preview_in_podman:
	bash ./scripts/run_in_podman.sh 'make preview'

.PHONY: build_in_podman
build_in_podman:
	bash ./scripts/run_in_podman.sh 'make build'
